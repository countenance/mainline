BEGIN;
CREATE SCHEMA countenance;
SET search_path = countenance;

CREATE TABLE event_invites (
	event_id integer,
	invite_email varchar(127) not null,
	invite_code varchar(127) not null,
	invite_click boolean default false not null,
	invite_rsvp boolean default false not null,
	invite_bringing integer not null
);

CREATE TABLE events (
	event_id integer primary key,
	event_title varchar(255) not null,
	event_details text not null
);

COMMIT;