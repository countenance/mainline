<?php

/* GNUkebox -- a free software server for recording your listening habits

   Copyright (C) 2009 Free Software Foundation, Inc

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

require_once('libs/database.php');
require_once('template/_top.inc');

if (isset($_POST["submit"])) {

  $invite_rsvp = $_POST['rsvp'];
  $invite_bringing = $_POST['bringing'];
  $invite_code = $_POST['code'];
  $event_id = $_POST['id'];

  if ($invite_rsvp == "") { $invite_rsvp = 0; }

	  $sql = "UPDATE event_invites SET invite_rsvp =" .$invite_rsvp .",invite_bringing = " . $invite_bringing ." WHERE event_id = " . $event_id . " AND invite_code = " . $adodb->qstr($invite_code);

	  $foo = $adodb->Execute($sql);

	  echo "<p>Thanks for responding!";

	  if ($invite_rsvp == 1) {
	    echo " We'll see you there!";
	  } else {
	    echo " Sorry you couldn't make it.";
	  }

	  echo "</p>";
  

} else {

if (isset($_GET['eid']) && isset($_GET['me'])) {

  // Look the person up in the database

  $event_id = $_GET['eid'];
  $invite_code = $_GET['me'];

  echo $invite_code;

  	$adodb->SetFetchMode(ADODB_FETCH_ASSOC);

	try { 
	  $res = $adodb->GetOne('SELECT event_title FROM events WHERE event_id = ' . $event_id);

	} catch (Exception $e) {
	  die($e->getMessage());
	}

        if ($res) {
	  echo "<h1>" . $res . "</h1>";
	  echo "<hr />";
	}

	try {

	  $res = $adodb->GetOne('SELECT * FROM event_invites WHERE event_id = ' . $event_id . ' AND invite_code = ' . $adodb->qstr($invite_code));

	} catch (Exception $e) {
		die($e->getMessage());
	}

        if ($res) {
     
	// Save a click through

	  $sql = "UPDATE event_invites SET invite_click=1 WHERE event_id = " . $event_id . " AND invite_code = " . $adodb->qstr($invite_code);

	$foo = $adodb->Execute($sql);

  // Display a pretty form

	require_once('template/_rsvp.inc');
	echo "<input type=hidden name=id value=" . $event_id . " />";
	echo "<input type=hidden name=code value=" . $invite_code . " />";
	echo "<input type=submit name=submit value='RSVP' />";
	echo "</form>";

	} else {

	  echo "<p>Sorry, we can't find you in the system. Please check with the event organizer.</p>";

	} 

} else {

  echo "<h1>No event selected</h1>";

  echo "<p>If you got here via a link in an email or instant message, check the link again...</p>";

  

  // Nobody is clicking through, display a very basic information screen

}}

require_once('template/_base.inc');

