<?php

/* GNUkebox -- a free software server for recording your listening habits

   Copyright (C) 2009 Free Software Foundation, Inc

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

require_once('libs/database.php');
require_once('template/_top.inc');

?>

<h1>Events admin</h1>

<hr />

<ul>
<li><a href="admin.php?mode=add">Add an event</a></li>
  <li><a href="admin.php?mode=list">See a list of events (edit/delete events, add/delete people)</a></li>
</ul>

<?php

  $mode = $_GET['mode'];

  switch ($mode) {

  case "list":

    $adodb->SetFetchMode(ADODB_FETCH_ASSOC);

    try {

      $res = $adodb->GetAll('SELECT * FROM events');

    } catch (Exception $e) {

      die($e->getMessage());

    }

    echo "<h2>List of all events</h2>";

    echo "<ul>\n";

    foreach ($res as &$row){

      echo "<li>" . $row['event_title'] . "</li>\n";

    }

    echo "</ul>";

  }

require_once('template/_base.inc');

?>
